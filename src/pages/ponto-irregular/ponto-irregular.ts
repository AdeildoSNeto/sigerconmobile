import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the PontoIrregularPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-ponto-irregular',
  templateUrl: 'ponto-irregular.html',
})
export class PontoIrregularPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera, private app: App) {
  }



  private foto: string; 
  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }


  tirarFoto(){
    this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000
    }).then((imageData) => {
      // imageData is a base64 encoded string
        this.foto = "data:image/jpeg;base64," + imageData;
    }, (err) => {
        console.log(err);
    });
  }

  sair()
  {
    this.app.getRootNav().setRoot("LoginPage");
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PontoIrregularPage');
  }

}
