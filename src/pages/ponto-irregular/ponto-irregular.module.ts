import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PontoIrregularPage } from './ponto-irregular';

@NgModule({
  declarations: [
    PontoIrregularPage,
  ],
  imports: [
    IonicPageModule.forChild(PontoIrregularPage),
  ],
  exports: [
    PontoIrregularPage
  ]
})
export class PontoIrregularPageModule {}
