import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuFiscalPage } from './menu-fiscal';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    MenuFiscalPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuFiscalPage),
    ComponentsModule
    
  ],
  exports: [
    MenuFiscalPage
  ]
})
export class MenuFiscalPageModule {}
