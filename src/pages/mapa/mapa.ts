
 import { Component } from "@angular/core/";
import { IonicPage,NavController, App } from 'ionic-angular';
 
 @IonicPage()
 @Component({
   selector: 'page-mapa',
   templateUrl: 'mapa.html'
 })
 export class MapaPage {
  

  constructor(private navCtrl: NavController, private app: App)
  {

  }

  sair()
  {
    this.app.getRootNav().setRoot("LoginPage");
  
  }
 }