import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MostrarDenunciaPage } from './mostrar-denuncia';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    MostrarDenunciaPage,
  ],
  imports: [
    IonicPageModule.forChild(MostrarDenunciaPage),
    ComponentsModule
  ],
  exports: [
    MostrarDenunciaPage
  ]
})
export class MostrarDenunciaPageModule {}
