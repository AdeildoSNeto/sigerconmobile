import { Component } from '@angular/core';
import { App,NavController, NavParams, ToastController, ActionSheetController } from 'ionic-angular';
import { Denuncia} from '../../models/denuncia.interface';
import {FirebaseListObservable, AngularFireDatabase} from 'angularfire2/database-deprecated';
import { Observable } from 'rxjs/Observable';



@Component({
  selector: 'app-menu-fiscal',
  templateUrl: 'menu-fiscal.component.html',
 
})
export class MenuFiscalPage {



  //denunciaListRef$: AngularFireList<Denuncia>;

  //denunciaList: Observable<Denuncia[]>;

  denunciaListRef$: FirebaseListObservable<Denuncia[]>;



  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private toast: ToastController, 
    private database: AngularFireDatabase,
    private app: App,
  private actionSheetCtrl: ActionSheetController) {

     //this.denunciaListRef$ = this.database.list('denuncia');  

    /*
     this.denunciaList = this.denunciaListRef$.snapshotChanges().map(changes => {

       return changes.map(c => ({ key: c.payload.key, ...c.payload.val()}));
     
      })
    
     console.log("DenunciaRef: " + this.denunciaListRef$);
    console.log("denuncialist: " + this.denunciaList.subscribe(c => console.log(c)));

    */
    this.denunciaListRef$ = this.database.list('denuncia');  
    

  }


selecionarDenuncia(denuncia: Denuncia)
{
  this.actionSheetCtrl.create({
    title: "Denúncia Pendente",
    buttons: [
      {
        text:'Abrir',
        handler: () => {
      

          this.navCtrl.push('MostrarDenunciaPage', {denunciaId: denuncia.$key});
        }
      },
        {
          text: 'Voltar',
          role: 'cancel',
          handler: () =>
          {
            console.log("Usuario Cancelou a denuncia");
          }
        }
      
    ]
  }).present();
}



  
}
