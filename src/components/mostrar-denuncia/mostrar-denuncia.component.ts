import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable} from 'angularfire2/database-deprecated';
import { Denuncia } from '../../models/denuncia.interface';
import {AlertController} from 'ionic-angular';



@Component({
  selector: 'app-mostrar-denuncia',
  templateUrl: 'mostrar-denuncia.component.html'
})
export class MostrarDenunciaComponent {

  denuncia = {} as Denuncia;
  
   denunciaObjRef$: FirebaseObjectObservable<Denuncia>;
  
  
  constructor(
    private database: AngularFireDatabase, 
    private navCtrl: NavController, 
    private navParams: NavParams,
    private alertCtrl: AlertController) {
    
      const denunciaId = this.navParams.get('denunciaId');
      
      console.log(denunciaId);
          this.denunciaObjRef$ = this.database.object(`denuncia/${denunciaId}`);
      
          this.denunciaObjRef$.subscribe(denuncia => this.denuncia = denuncia);


    //console.log(this.denunciaObjRef.valueChanges().subscribe());
  }


  
  confirmarDenuncia()
  {
    let confirma = this.alertCtrl.create({
      title: "Validar Denúncia?",
      message: "Deseja realmente validar esta denúncia? Após a confirmação, seus dados serão transferidos para a área de Pontos Irregulares.",
      buttons: [{
        text: 'Cancelar',
        handler: () => {
          console.log("Validação Cancelada");
         
          

          
        }
      },
        {
        text: 'Confirmar',
        handler: () =>{
          this.denuncia.status = "Validada";
          this.denunciaObjRef$.update(this.denuncia);
         console.log("Validação enviada");
         console.log(this.denuncia)
          
        }
      }
    ]
  
    });
    confirma.present();
    
  }


  descartarDenuncia()
  {
    let confirma = this.alertCtrl.create({
      title: "Descartar Denúncia?",
      message: "Deseja realmente descartar a denúncia? Após o descarte, todos os dados sobre esta denúncia serão perdidos",
      buttons: [{
        text: 'Cancelar',
        handler: () => {
          console.log("Descarte Cancelado");
        }
      },
        {
        text: 'Descartar',
        handler: () =>{
          this.denunciaObjRef$.remove();
         console.log("Descarte confirmado");
         this.navCtrl.pop();
          
        }
      }
    ]
  
    });
    confirma.present();
  
  }



}









