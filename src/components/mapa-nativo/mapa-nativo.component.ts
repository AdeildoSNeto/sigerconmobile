import { Component } from '@angular/core';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
 } from '@ionic-native/google-maps';


@Component({
  selector: 'app-mapa-nativo',
  templateUrl: 'mapa-nativo.component.html'
})
export class MapaNativoComponent {

  mapa: GoogleMap;
  mapaElement: HTMLElement;

  constructor(private googleMaps: GoogleMaps) {
  
  }

  carregarMapa() {

    console.log("map is here?");
    this.mapaElement = document.getElementById('mapa');

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 43.0741904,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.mapa = new GoogleMap(this.mapaElement, mapOptions);

    this.mapa.one(GoogleMapsEvent.MAP_READY)
    .then(() => {
      console.log('Map is ready!');


      this.mapa.addMarker({
        title: 'Ionic',
        icon: 'blue',
        animation: 'DROP',
        position: {
          lat: 43.0741904,
          lng: -89.3809802
        }
      })
      .then(marker => {
        marker.on(GoogleMapsEvent.MARKER_CLICK)
          .subscribe(() => {
            alert('clicked');
          });
      });


    });
  }


onload = this.carregarMapa();
}
