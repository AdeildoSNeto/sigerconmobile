import { Component,ViewChild, ElementRef } from '@angular/core';

declare var google;


@Component({
  selector: 'app-mapa',
  templateUrl: 'mapa.component.html'
})
export class MapaComponent {

  @ViewChild('map') mapElement: ElementRef;
  mapa: any;
  
  constructor() {
 
  }


  carregarMapa(){
    
    console.log("teste");
    console.log(this.mapElement);

       let latLng = new google.maps.LatLng(-34.9290, 138.6010);
    
       let mapOptions = {
         center: latLng,
         zoom: 15,
         mapTypeId: google.maps.MapTypeId.ROADMAP
       }
    
       this.mapa = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    
     }



ionViewDidLoad()
{
  console.log("ionview");

  this.carregarMapa();
}

}
