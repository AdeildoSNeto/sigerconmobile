import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { Denuncia} from '../../models/denuncia.interface';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database-deprecated';
import {AlertController} from 'ionic-angular';

@Component({
  selector: 'app-denuncia-form',
  templateUrl: 'denuncia-form.component.html',
})
export class DenunciaFormComponent {

  private foto: string; 
  denunciaRef$: FirebaseListObservable<Denuncia[]>;

  denuncia = {} as Denuncia;


  constructor(private navCtrl: NavController, 
    private navParams: NavParams, 
    private camera: Camera, 
    private database: AngularFireDatabase, 
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController) {
    
    
      this.denunciaRef$ = this.database.list('denuncia');
      this.denuncia.entorno = [];
      this.denuncia.tipo = [];  
  }



  //ISTO É UMA GAMBIARRA, LOGO ACHAREI UM MODO CORRETO DE FAZER
  // ---------- INÍCIO DA GAMBIARRA ----------
  casa: boolean = null;
  comercio: boolean = null;
  escola: boolean = null;
  hospital: boolean = null;
  vegetacao: boolean = null;
  rio: boolean = null;

  entulho: boolean = null;
  organico: boolean = null;
  poda: boolean = null;


confirmarDenuncia()
{
  let confirma = this.alertCtrl.create({
    title: "Enviar a denúncia?",
    message: "Você deseja realmente enviar esta denúncia?",
    buttons: [{
      text: 'Cancelar',
      handler: () => {
        console.log("Denuncia Cancelada");
      }
    },
      {
      text: 'Enviar',
      handler: () =>{
        this.enviarDenuncia(this.denuncia);
        this.navCtrl.push('LoginPage');
        
      }
    }
  ]

  });
  confirma.present();
  
}
 
validarFormEntorno(casa, escola, hospital, comercio, vegetacao, rio)
{
  if(casa==true)
  {
    this.denuncia.entorno.push("Casa");
  }
  if(escola==true)
  {
    this.denuncia.entorno.push("Escola");
  }
  if(hospital==true)
  {
    this.denuncia.entorno.push("Hospital");
  }
  if(comercio==true)
  {
    this.denuncia.entorno.push("Comércio");
  }
  if(vegetacao==true)
  {
    this.denuncia.entorno.push("Vegetação");
  }
  if(rio==true)
  {
    this.denuncia.entorno.push("Rio");
  }
}
   
validarFormTipo(entulho, organico, poda)
{
  if(entulho == true)
  {
    this.denuncia.tipo.push("Entulho")
  }
  if(organico == true)
  {
    this.denuncia.tipo.push("Organico");
  }
  if(poda == true)
  {
    this.denuncia.tipo.push("Poda");
  }
}

// ---------------------- FIM DA GAMBIARRA -------------------------




 
  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }


tirarFoto(){
    this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000
    }).then((imageData) => {
      // imageData is a base64 encoded string
        this.foto = "data:image/jpeg;base64," + imageData;
    }, (err) => {
        console.log(err);
    });
  }




  enviarDenuncia(denuncia: Denuncia)
  {
    this.validarFormEntorno(this.casa, this.escola,this.hospital,this.comercio,this.vegetacao,this.rio);
    
    this.validarFormTipo(this.entulho, this.organico, this.poda);

    this.denuncia.status = "Pendente";

    this.denuncia.foto = this.foto;
     
    if(this.denuncia.quantidade != null){

     if(this.denuncia.foto != null){

     
     this.denunciaRef$.push({
        quantidade: this.denuncia.quantidade,
        entorno: this.denuncia.entorno,
        tipo: this.denuncia.tipo,
        foto: this.denuncia.foto,
        status: this.denuncia.status
        
    });
  
    
   

  }else{

     
    this.denunciaRef$.push({
      quantidade: this.denuncia.quantidade,
      entorno: this.denuncia.entorno,
      tipo: this.denuncia.tipo,
      status: this.denuncia.status
      
  });

 
  
  } 
 
  }else
  {
    alert("O porte de pilhas é um campo obrigatório.");
  }
}
   
  


  ionViewDidLoad() {
    
   
   
  }

}
