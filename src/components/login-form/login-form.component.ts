import { Component } from '@angular/core';
import {NavController, NavParams, ToastController } from 'ionic-angular';


/**
 * Generated class for the LoginFormComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'app-login-form',
  templateUrl: 'login-form.component.html'
})
export class LoginFormComponent {

  text: string;

  constructor(private navCtrl: NavController, 
    private navParams: NavParams,
    private toast: ToastController, ) {
  
  }




 verificarLogin(): void
 {
   
      
     this.irParaPagina("TabsPage");
 
   
 
 
 }
 voltar(): void {
  this.navCtrl.pop();
 
 }
 
 irParaPagina(nomePagina: string)  
 {
   this.navCtrl.push(nomePagina);
 
 }
 
}
