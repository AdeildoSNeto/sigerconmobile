
import {NgModule} from '@angular/core';
import {IonicModule} from 'ionic-angular';
import {LoginFormComponent} from './login-form/login-form.component';
import {DenunciaFormComponent} from './denuncia-form/denuncia-form.component';
import {MenuFiscalPage} from './menu-fiscal/menu-fiscal.component';
import {MostrarDenunciaComponent} from './mostrar-denuncia/mostrar-denuncia.component';
//import { MapaComponent } from './mapa/mapa.component';
import {MapaNativoComponent} from './mapa-nativo/mapa-nativo.component';
@NgModule({
    declarations: [LoginFormComponent,
        DenunciaFormComponent,
        MenuFiscalPage,
        MostrarDenunciaComponent,
      //  MapaComponent
      MapaNativoComponent


    ],
    imports: [IonicModule],
    exports: [LoginFormComponent,
        DenunciaFormComponent,
        MenuFiscalPage,
        MostrarDenunciaComponent,
       // MapaComponent
       MapaNativoComponent
    ]
})

export class ComponentsModule {

}